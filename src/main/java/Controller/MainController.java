package Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController
{
	@GetMapping("/home")
	public String Home()
	{
		return "Pagina inicial";
	}
	
	@GetMapping("/admin")
	public String Admin()
	{
		return "Painel administrativo";
	}
}

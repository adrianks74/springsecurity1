package com.adrian.SpringSecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityApplication.class, args);
	}
	
	/*
	 *
	 *
	 *
	 insert into person (id, login, password, funcao) values (1, 'fabricio', '$2a$10$ARppQC0FRWaGP4pnZqYbpuVyYOWIp4q1r2ViT3PGYK6BafD5PXFiS', 'ADMIN');
insert into person (id, login, password, funcao) values (2, 'grazi', '$2a$10$ARppQC0FRWaGP4pnZqYbpuVyYOWIp4q1r2ViT3PGYK6BafD5PXFiS', 'ADMIN');
insert into person (id, login, password, funcao) values (3, 'felipe', '$2a$10$ARppQC0FRWaGP4pnZqYbpuVyYOWIp4q1r2ViT3PGYK6BafD5PXFiS', 'USER');
	 
	 
	 * 
	 * */
}

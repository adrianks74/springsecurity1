package Services;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import Model.Person;

public class PersonDetails implements UserDetails
{
	private Person person;
	
	public PersonDetails(Person p)
	{
		super();
		this.person = p;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		return Collections.singleton( new SimpleGrantedAuthority( person.getFuncao() ) );
	}

	@Override
	public String getPassword()
	{
		return person.getPassword();
	}

	@Override
	public String getUsername()
	{
		return person.getLogin();
	}

	@Override
	public boolean isAccountNonExpired()
	{
		return true;
	}

	@Override
	public boolean isAccountNonLocked()
	{
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	@Override
	public boolean isEnabled()
	{
		return true;
	}

}

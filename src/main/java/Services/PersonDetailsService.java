package Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import Model.Person;
import Repositories.PersonRepository;

@Service
public class PersonDetailsService implements UserDetailsService
{
	@Autowired
	private PersonRepository personRepo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
	{
		Person person = personRepo.findByLogin( username );
		
		if(person == null)
			throw new UsernameNotFoundException(username+" not found!");

		return new PersonDetails(person);
	}

}
